# ice-[tree](https://www.archlinux.org/packages/extra/x86_64/tree/)

Recursive directory listing. http://mama.indstate.edu/users/ice/tree/

## Demo
* [pacman-packages-demo.gitlab.io/ice-tree/
  ](https://pacman-packages-demo.gitlab.io/ice-tree/)
* [job=tree](https://gitlab.com/pacman-packages-demo/ice-tree/-/jobs/artifacts/master/file/index.html?job=tree)